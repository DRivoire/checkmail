<?php
const EMAIL_PARAM_NAME = 'email';
const SPAM_DOMAINS = ['spamming.com', 'mailinator.com', 'oneminutemail.com'];

//Retourne l'adresse mail en param
function getMail() {
  if ( empty($_GET) || empty($_GET[EMAIL_PARAM_NAME]) ) {
    echo 'Please provide valid mail address';
    exit;
  } else {
    return $_GET[EMAIL_PARAM_NAME];
  }
}

//Vérifier l'adresse mail en param
function testMail($email) {
  if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $emailParts = explode('@', $email);
    return !in_array($emailParts[1], SPAM_DOMAINS);
  } else {
    echo 'Invalid email address';
    exit;
  }
}

//Ecrire si passe les tests
if ( testMail(getMail())) {
  echo 'Email is valid';
} else {
  echo 'Email is spam';
}